package net.vintage.ticketsystem.config;

import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Configuration
@ConfigurationProperties("mongo")
@EnableMongoRepositories(basePackages = "net.vintage.ticketsystem.repository")
@EnableTransactionManagement
public class MongoConfig {
    private String credPath;
    private final MongoMappingContext mongoMappingContext;

    public MongoConfig(MongoMappingContext mongoMappingContext) {
        this.mongoMappingContext = mongoMappingContext;
    }

    @Bean
    @Primary
    @SneakyThrows
    public MongoDatabaseFactory mongoDbFactory() {
        final var collect = Files.lines(Paths.get(credPath), StandardCharsets.UTF_8).collect(Collectors.joining());
        return new SimpleMongoClientDatabaseFactory(collect);
    }

    @Bean
    public MongoTemplate mongoTemplate(MongoDatabaseFactory mongoDbFactory) {
        var dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
        final var converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        return new MongoTemplate(mongoDbFactory, converter);
    }

    @Bean
    MongoTransactionManager transactionManager(MongoDatabaseFactory dbFactory) {
        return new MongoTransactionManager(dbFactory);
    }

    public String getCredPath() {
        return credPath;
    }

    public void setCredPath(String credPath) {
        this.credPath = credPath;
    }
}
