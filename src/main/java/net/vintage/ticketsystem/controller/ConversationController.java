package net.vintage.ticketsystem.controller;

import net.vintage.ticketsystem.domain.ConversationMessage;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.concurrent.Callable;

public interface ConversationController {
    Callable<ResponseEntity<List<ConversationMessage>>> getConversationByTicketId(String ticketId);

    Callable<ResponseEntity<ConversationMessage>> sendMessage(ConversationMessage conversationMessage);

    Callable<ResponseEntity<Boolean>> deleteMessage(String messageId);

    Callable<ResponseEntity<ConversationMessage>> editMessage(ConversationMessage conversationMessage);
}
