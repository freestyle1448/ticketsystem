package net.vintage.ticketsystem.repository;

import net.vintage.ticketsystem.domain.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PhotoRepository extends MongoRepository<Photo, String> {
}
