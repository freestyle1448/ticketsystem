package net.vintage.ticketsystem.service;

import lombok.RequiredArgsConstructor;
import net.vintage.ticketsystem.domain.Ticket;
import net.vintage.ticketsystem.exception.NotFoundException;
import net.vintage.ticketsystem.repository.TicketRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Async
@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {
    private final TicketRepository ticketRepository;

    @Override
    @Transactional
    public CompletableFuture<Ticket> createTicket(Ticket ticket) {
        return CompletableFuture.completedFuture(ticketRepository.save(ticket));
    }

    @Override
    @Transactional(readOnly = true)
    public CompletableFuture<Ticket> getTicketById(String ticketId) {
        return CompletableFuture.completedFuture(ticketRepository.findById(ticketId)
                .orElseThrow((Supplier<RuntimeException>) () -> new NotFoundException("Ticket not found")));
    }

    @Override
    @Transactional(readOnly = true)
    public CompletableFuture<List<Ticket>> getAllTickets() {
        return CompletableFuture.completedFuture(ticketRepository.findAll());
    }

    @Override
    @Transactional
    public CompletableFuture<Boolean> deleteTicket(String ticketId) {
        ticketRepository.deleteById(ticketId);
        return CompletableFuture.completedFuture(true);
    }

    @Override
    @Transactional
    public CompletableFuture<Ticket> updateTicket(Ticket ticket) {
        if (ticket.getId() == null) {
            throw new NotFoundException("Ticket not found");
        }

        return CompletableFuture.completedFuture(ticketRepository.save(ticket));
    }
}
