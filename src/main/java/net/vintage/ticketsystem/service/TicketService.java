package net.vintage.ticketsystem.service;

import net.vintage.ticketsystem.domain.Ticket;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface TicketService {
    CompletableFuture<Ticket> createTicket(Ticket ticket);

    CompletableFuture<Ticket> getTicketById(String ticketId);

    CompletableFuture<List<Ticket>> getAllTickets();

    CompletableFuture<Boolean> deleteTicket(String ticketId);

    CompletableFuture<Ticket> updateTicket(Ticket ticket);
}
