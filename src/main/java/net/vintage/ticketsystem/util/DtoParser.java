package net.vintage.ticketsystem.util;

import net.vintage.ticketsystem.domain.Photo;
import net.vintage.ticketsystem.domain.Ticket;
import net.vintage.ticketsystem.domain.TicketStatus;
import net.vintage.ticketsystem.dto.TicketDto;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

@Component
public class DtoParser {
    public Ticket ticketDtoToPojo(TicketDto ticketDto) {
        final var photos = new ArrayList<Photo>();

        if (ticketDto.getPhotos() != null) {
            for (MultipartFile photo : ticketDto.getPhotos()) {
                try {
                    final var bytes = photo.getBytes();
                    if (bytes.length == 0) {
                        continue;
                    }

                    photos.add(Photo.builder()
                            .title(photo.getName())
                            .image(new Binary(BsonBinarySubType.BINARY, photo.getBytes()))
                            .build());
                } catch (IOException e) {
                    return Ticket.builder()
                            .id(ticketDto.getId())
                            .name(ticketDto.getName())
                            .lastModifyDate(new Date())
                            .description(ticketDto.getDescription())
                            .userId(ticketDto.getUserId())
                            .status(TicketStatus.NEW)
                            .build();
                }
            }
        }

        return Ticket.builder()
                .id(ticketDto.getId())
                .name(ticketDto.getName())
                .lastModifyDate(new Date())
                .description(ticketDto.getDescription())
                .userId(ticketDto.getUserId())
                .photos(photos)
                .status(TicketStatus.NEW)
                .build();
    }
}
