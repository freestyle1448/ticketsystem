package net.vintage.ticketsystem.controller;

import net.vintage.ticketsystem.domain.Ticket;
import net.vintage.ticketsystem.dto.TicketDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.concurrent.Callable;

public interface TicketController {
    Callable<ResponseEntity<Ticket>> createTicket(TicketDto ticketDto);

    Callable<ResponseEntity<Ticket>> getTicket(String ticketId);

    Callable<ResponseEntity<List<Ticket>>> getAllTickets();

    Callable<ResponseEntity<Boolean>> deleteTicket(String ticketId);

    Callable<ResponseEntity<Ticket>> updateTicket(TicketDto ticketDto);
}
