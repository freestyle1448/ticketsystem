package net.vintage.ticketsystem.controller;

import lombok.RequiredArgsConstructor;
import net.vintage.ticketsystem.domain.ConversationMessage;
import net.vintage.ticketsystem.service.ConversationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequiredArgsConstructor
public class ConversationControllerImpl implements ConversationController {
    private final ConversationService conversationService;

    @Override
    @GetMapping("/java/conversation/{ticket_id}")
    public Callable<ResponseEntity<List<ConversationMessage>>> getConversationByTicketId(@PathVariable("ticket_id") String ticketId) {
        return () -> ResponseEntity.ok(conversationService.getConversationByTicketId(ticketId).get());
    }

    @Override
    @PostMapping("/java/conversation")
    public Callable<ResponseEntity<ConversationMessage>> sendMessage(ConversationMessage conversationMessage) {
        conversationMessage.setDate(new Date());

        return () -> ResponseEntity.ok(
                conversationService.addMessage(conversationMessage).get());
    }

    @Override
    @DeleteMapping("/java/conversation/{id}")
    public Callable<ResponseEntity<Boolean>> deleteMessage(@PathVariable("id") String messageId) {
        return () -> ResponseEntity.ok(conversationService.deleteMessage(messageId).get());
    }

    @Override
    @PutMapping("/java/conversation")
    public Callable<ResponseEntity<ConversationMessage>> editMessage(ConversationMessage conversationMessage) {
        return () -> ResponseEntity.ok(conversationService.editMessage(conversationMessage).get());
    }
}
