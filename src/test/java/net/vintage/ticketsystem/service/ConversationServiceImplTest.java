package net.vintage.ticketsystem.service;

import lombok.SneakyThrows;
import net.vintage.ticketsystem.domain.ConversationMessage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Import(ConversationServiceImpl.class)
@DisplayName("Сервис для работы с сообщениями:")
@DataMongoTest
class ConversationServiceImplTest {
    @Autowired
    private ConversationServiceImpl conversationService;
    @Autowired
    private MongoTemplate template;

    @BeforeEach
    void init() {
        final var doc1 = ConversationMessage.builder()
                .date(new Date())
                .id("1")
                .message("Помогите с проблемой!")
                .ticketId("123")
                .userId("user")
                .build();
        final var doc2 = ConversationMessage.builder()
                .date(new Date())
                .id("2")
                .message("Помогите с проблемой1!")
                .ticketId("123")
                .userId("user3")
                .build();
        final var doc3 = ConversationMessage.builder()
                .date(new Date())
                .id("3")
                .message("Помогите с проблемой2!")
                .ticketId("1234")
                .userId("user2")
                .build();
        final var doc4 = ConversationMessage.builder()
                .date(new Date())
                .id("4")
                .message("Помогите с проблемой3!")
                .ticketId("1234")
                .userId("user1")
                .build();

        template.insertAll(List.of(doc1, doc2, doc3, doc4));
    }

    @AfterEach
    void clear() {
        template.dropCollection(ConversationMessage.class);
    }

    @Test
    @DisplayName("должен создавать новое сообщение")
    void addMessage() {
        final var testMessage = ConversationMessage.builder()
                .date(new Date())
                .id("5")
                .message("testText")
                .ticketId("12")
                .userId("user5")
                .build();

        conversationService.addMessage(testMessage);

        final var conversationMessageFromDb =
                template.findOne(new Query(Criteria.where("id").is("5")), ConversationMessage.class);

        assertThat(conversationMessageFromDb).isNotNull()
                .matches(conversationMessage
                        -> conversationMessage.getId().equals(testMessage.getId()))
                .matches(conversationMessage -> conversationMessage.getMessage().equals("testText"));
    }

    @Test
    @DisplayName("должен удалять сообщение")
    void deleteMessage() {
        conversationService.deleteMessage("1");

        final var message = template.findOne(new Query(Criteria.where("id").is("1")), ConversationMessage.class);

        assertThat(message).isNull();
    }

    @Test
    @DisplayName("должен менять сообщения")
    void editMessage() {
    }

    @Test
    @DisplayName("должен получить все сообщения по тикету")
    @SneakyThrows
    void getConversationByTicketId() {
        final var conversations = conversationService.getConversationByTicketId("123").get();

        assertThat(conversations).hasSize(2)
                .anyMatch(conversation ->
                        conversation.getId().equals("1") && conversation.getUserId().equals("user") &&
                                conversation.getMessage().equals("Помогите с проблемой!"))
                .anyMatch(conversation ->
                        conversation.getId().equals("2") && conversation.getUserId().equals("user3") &&
                                conversation.getMessage().equals("Помогите с проблемой1!"));
    }
}