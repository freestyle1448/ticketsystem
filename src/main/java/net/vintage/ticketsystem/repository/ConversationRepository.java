package net.vintage.ticketsystem.repository;

import net.vintage.ticketsystem.domain.ConversationMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ConversationRepository extends MongoRepository<ConversationMessage, String> {
    List<ConversationMessage> getConversationMessageByTicketIdOrderByDate(String ticketId);
}
