package net.vintage.ticketsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@Builder
@AllArgsConstructor
public class TicketDto {
    private String id;
    private String name;
    private String description;
    private String date;
    private String userId;
    private MultipartFile[] photos;
}
