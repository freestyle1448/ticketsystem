package net.vintage.ticketsystem.domain;

public final class TicketStatus {
    public static final int NEW = 0;
    public static final int IN_WORK = 1;
    public static final int SUCCESS = 2;
}
