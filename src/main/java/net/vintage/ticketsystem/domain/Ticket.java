package net.vintage.ticketsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document(collection = "tickets")
@AllArgsConstructor
public class Ticket {
    @Id
    private String id;
    private Integer status;
    private String name;
    private String description;
    private Date createDate;
    private Date lastModifyDate;
    private String userId;
    private List<Photo> photos;
}
