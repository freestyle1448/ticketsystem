package net.vintage.ticketsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@Document(collection = "conversation")
@AllArgsConstructor
public class ConversationMessage {
    @Id
    private String id;
    private String userId;
    private String ticketId;
    private String message;
    private Date date;
}
