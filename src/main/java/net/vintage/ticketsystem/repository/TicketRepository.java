package net.vintage.ticketsystem.repository;

import net.vintage.ticketsystem.domain.Ticket;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TicketRepository extends MongoRepository<Ticket, String> {
}
