package net.vintage.ticketsystem.service;

import net.vintage.ticketsystem.domain.ConversationMessage;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ConversationService {
    CompletableFuture<ConversationMessage> addMessage(ConversationMessage conversationMessage);

    CompletableFuture<Boolean> deleteMessage(String messageId);

    CompletableFuture<ConversationMessage> editMessage(ConversationMessage conversationMessage);

    CompletableFuture<List<ConversationMessage>> getConversationByTicketId(String ticketId);
}
