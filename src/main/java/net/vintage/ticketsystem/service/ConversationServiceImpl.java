package net.vintage.ticketsystem.service;

import lombok.RequiredArgsConstructor;
import net.vintage.ticketsystem.domain.ConversationMessage;
import net.vintage.ticketsystem.exception.NotFoundException;
import net.vintage.ticketsystem.repository.ConversationRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Async
@Service
@RequiredArgsConstructor
public class ConversationServiceImpl implements ConversationService {
    private final ConversationRepository conversationRepository;

    @Override
    @Transactional
    public CompletableFuture<ConversationMessage> addMessage(ConversationMessage conversationMessage) {
        return CompletableFuture.completedFuture(conversationRepository.save(conversationMessage));
    }

    @Override
    @Transactional
    public CompletableFuture<Boolean> deleteMessage(String messageId) {
        conversationRepository.deleteById(messageId);

        return CompletableFuture.completedFuture(true);
    }

    @Override
    @Transactional
    public CompletableFuture<ConversationMessage> editMessage(ConversationMessage conversationMessage) {
        if (conversationMessage.getId() == null) {
            throw new NotFoundException("Message not found");
        }

        return CompletableFuture.completedFuture(conversationRepository.save(conversationMessage));
    }

    @Override
    @Transactional(readOnly = true)
    public CompletableFuture<List<ConversationMessage>> getConversationByTicketId(String ticketId) {
        return CompletableFuture.completedFuture(
                conversationRepository.getConversationMessageByTicketIdOrderByDate(ticketId));
    }
}
