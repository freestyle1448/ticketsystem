package net.vintage.ticketsystem.service;


import lombok.SneakyThrows;
import net.vintage.ticketsystem.domain.Ticket;
import net.vintage.ticketsystem.exception.NotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Import(TicketServiceImpl.class)
@DisplayName("Сервис для работы с тикетами:")
@DataMongoTest
class TicketServiceImplTest {
    @Autowired
    private TicketServiceImpl ticketService;
    @Autowired
    private MongoTemplate template;

    @BeforeEach
    void init() {
        final var doc1 = Ticket.builder()
                .id("1")
                .name("ticket1")
                .userId("user1")
                .createDate(new Date())
                .build();

        final var doc2 = Ticket.builder()
                .id("2")
                .name("ticket2")
                .userId("user2")
                .createDate(new Date())
                .build();

        template.insertAll(List.of(doc1, doc2));
    }

    @AfterEach
    void clear() {
        template.dropCollection(Ticket.class);
    }

    @Test
    @DisplayName("должен создать новый тикет")
    void createTicket() {
        ticketService.createTicket(Ticket.builder()
                .createDate(new Date())
                .id("3")
                .name("ticket3")
                .userId("user3")
                .build());

        final var ticketFromDb = template.findOne(new Query(Criteria.where("id").is("3")), Ticket.class);

        assertThat(ticketFromDb).isNotNull()
                .matches(ticket -> ticket.getId().equals("3"))
                .matches(ticket -> ticket.getName().equals("ticket3"))
                .matches(ticket -> ticket.getUserId().equals("user3"));
    }


    @Test
    @DisplayName("получить тикет по id")
    @SneakyThrows
    void getTicketById() {
        final var ticketFromDb = ticketService.getTicketById("2").get();

        assertThat(ticketFromDb).isNotNull()
                .matches(ticket -> ticket.getId().equals("2"))
                .matches(ticket -> ticket.getName().equals("ticket2"))
                .matches(ticket -> ticket.getUserId().equals("user2"));
    }

    @Test
    @DisplayName("получить все тикеты")
    @SneakyThrows
    void getAllTickets() {
        final var tickets = ticketService.getAllTickets().get();

        assertThat(tickets).hasSize(2)
                .anyMatch(ticket ->
                        ticket.getId().equals("1") && ticket.getUserId().equals("user1") &&
                                ticket.getName().equals("ticket1"))
                .anyMatch(ticket ->
                        ticket.getId().equals("2") && ticket.getUserId().equals("user2") &&
                                ticket.getName().equals("ticket2"));
    }

    @Test
    @DisplayName("должен удалить тикет по id")
    void deleteTicket() {
        ticketService.deleteTicket("1");

        final var ticket = template.findOne(new Query(Criteria.where("id").is("1")), Ticket.class);

        assertThat(ticket).isNull();
    }

    @Test
    @DisplayName("должен обновить тикет")
    void updateTicket() {
        ticketService.updateTicket(Ticket.builder()
                .createDate(new Date())
                .id("2")
                .name("ticket3")
                .userId("user3")
                .photos(Collections.emptyList())
                .build());

        final var ticketFromDb = template.findOne(new Query(Criteria.where("id").is("2")), Ticket.class);

        assertThat(ticketFromDb).isNotNull()
                .matches(ticket -> ticket.getId().equals("2"))
                .matches(ticket -> ticket.getName().equals("ticket3"))
                .matches(ticket -> ticket.getUserId().equals("user3"))
                .matches(ticket -> ticket.getPhotos() != null);
    }

    @Test
    @DisplayName("должен получить NotFoundException при попытке обновление несуществуюшего тикета")
    void updateNonExistTicket() {
        assertThatThrownBy(() -> ticketService.updateTicket(Ticket.builder()
                .createDate(new Date())
                .name("ticket3")
                .userId("user3")
                .photos(Collections.emptyList())
                .build())).isInstanceOf(NotFoundException.class)
                .hasMessageContaining("Ticket not found");
    }
}