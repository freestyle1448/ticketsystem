package net.vintage.ticketsystem.controller;

import lombok.RequiredArgsConstructor;
import net.vintage.ticketsystem.domain.Ticket;
import net.vintage.ticketsystem.dto.TicketDto;
import net.vintage.ticketsystem.service.TicketService;
import net.vintage.ticketsystem.util.DtoParser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequiredArgsConstructor
public class TicketControllerImpl implements TicketController {
    private final DtoParser dtoParser;
    private final TicketService ticketService;

    @Override
    @PostMapping("/java/ticket")
    public Callable<ResponseEntity<Ticket>> createTicket(TicketDto ticketDto) {
        final var ticket = dtoParser.ticketDtoToPojo(ticketDto);
        ticket.setCreateDate(new Date());

        return () -> ResponseEntity.ok(ticketService.createTicket(ticket).get());
    }

    @Override
    @GetMapping("/java/ticket/{ticket_id}")
    public Callable<ResponseEntity<Ticket>> getTicket(@PathVariable("ticket_id") String ticketId) {
        return () -> ResponseEntity.ok(ticketService.getTicketById(ticketId).get());
    }

    @Override
    @GetMapping("/java/ticket")
    public Callable<ResponseEntity<List<Ticket>>> getAllTickets() {
        return () -> ResponseEntity.ok(ticketService.getAllTickets().get());
    }

    @Override
    @DeleteMapping("/java/ticket/{ticket_id}")
    public Callable<ResponseEntity<Boolean>> deleteTicket(@PathVariable("ticket_id") String ticketId) {
        return () -> ResponseEntity.ok(ticketService.deleteTicket(ticketId).get());
    }

    @Override
    @PutMapping("/java/ticket")
    public Callable<ResponseEntity<Ticket>> updateTicket(TicketDto ticketDto) {
        final var ticket = dtoParser.ticketDtoToPojo(ticketDto);

        return () -> ResponseEntity.ok(ticketService.createTicket(ticket).get());
    }

}
